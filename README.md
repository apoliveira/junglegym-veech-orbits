# Jungle Gym Veech Orbits Search

Let's find those elements!

I've documented the main.py file as best as I can with the approach to how this works. Essentially it's a breadth-first-search on the space of words (i.e. matrices in the Veech group).

## Generators

Thanks Sunrose for finding these!

$A = \begin{align} 0 & -1 \\ 1 & 0 \end{align}$
$B = \begin{align} 1 & 4 \\ 0 & 1 \end{align}$
$C = \begin{align} 5 & -8 \\ 2 & -3 \end{align}$

## List of found elements

Below are the list of fractions, say $x$, that we have found a Veech element, $M$, so that $M x$ is one of the standard basis vectors. Thus, $M^{-1}x$ sends one of the basis vectors to $x$.

The arrays below are the words (read left-to-right) that multiply to $M$. That is, for the vector $(1,4)$ the matrix $M = BA$ sends $(1,4)$ to $(0,1)$.

> Perhaps this would be easier to read if it were TeXed as $BA$ or $A^{-1} B B A$ for the vector $(1,8)$.

- \frac{1}{4}: ['B', 'A']
- \frac{1}{8}: ['A1', 'B', 'B', 'A']
- \frac{1}{12}: ['A', 'B', 'B', 'B', 'A1']
- \frac{4}{15}: ['B', 'A1', 'B', 'A1']
- \frac{1}{16}: ['A', 'B', 'B', 'B', 'B', 'A1']
- \frac{4}{17}: ['B1', 'A1', 'B', 'A']
- \frac{13}{18}: ['C', 'A1', 'C1', 'B', 'A']
- \frac{1}{20}: ['A', 'B', 'B', 'B', 'B', 'B', 'A1']
- \frac{1}{24}: ['A', 'B', 'B', 'B', 'B', 'B', 'B', 'A1']
- \frac{18}{25}: ['B1', 'C', 'A', 'C1', 'B', 'A1']
- \frac{1}{28}: ['A', 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'A1']
- \frac{4}{31}: ['B', 'A1', 'B', 'B', 'A1']
- \frac{8}{31}: ['B', 'B', 'A1', 'B', 'A1']
- \frac{4}{33}: ['B1', 'A1', 'B', 'B', 'A']
- \frac{8}{33}: ['B1', 'B1', 'A1', 'B', 'A']
- \frac{4}{47}: ['B', 'A', 'B', 'B', 'B', 'A']
- \frac{12}{47}: ['B', 'B', 'B', 'A1', 'B', 'A1']
- \frac{34}{47}: ['C', 'A1', 'C1', 'B', 'A1']
- \frac{4}{49}: ['B1', 'A', 'B', 'B', 'B', 'A1']
- \frac{12}{49}: ['B1', 'B1', 'B1', 'A', 'B', 'A1']
- \frac{15}{56}: ['B', 'A', 'B', 'A', 'B', 'A']
- \frac{8}{63}: ['B', 'B', 'A', 'B', 'B', 'A']
- \frac{15}{64}: ['B1', 'A', 'B1', 'A', 'B', 'A']
- \frac{17}{64}: ['B1', 'A', 'B', 'A', 'B', 'A1']
- \frac{8}{65}: ['B1', 'B1', 'A', 'B', 'B', 'A1']
- \frac{13}{70}: ['C', 'A', 'C1', 'B', 'B', 'A1']
- \frac{29}{70}: ['C1', 'A1', 'C', 'B', 'A']
- \frac{17}{72}: ['B', 'A', 'B1', 'A', 'B', 'A1']
- \frac{46}{111}: ['C1', 'A1', 'C', 'B', 'A1']
- \frac{86}{119}: ['B', 'C', 'A', 'C1', 'B', 'A']
- \frac{34}{149}: ['C', 'A', 'C1', 'A', 'B', 'A']
- \frac{70}{169}: ['B1', 'C1', 'A', 'C', 'B', 'A1']
- \frac{34}{183}: ['C', 'A', 'C1', 'B', 'B', 'A']
- \frac{29}{186}: ['C1', 'A', 'C', 'B', 'B', 'A1']


### Technical comments

The data (.pkl) files got VERY large. I had to use git-lfs to push them to GitHub, so if you try to pull it with git you may have some issues and will need to install [git-lfs][https://git-lfs.github.com/]. Should be smooth sailing once you install that. You'll be able to just pull/push as normal.
