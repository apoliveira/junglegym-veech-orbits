import numpy as np
from numpy.linalg import inv

import pickle

# Setup matrices
ID = np.array([ [1,0],[0,1] ])
e0 = np.array([ [1], [0] ])
e1 = np.array([ [0], [1] ])

A = np.array([ [0,-1],[1,0] ])
A1 = inv(A)
B = np.array([ [1,4],[0,1] ])
B1 = inv(B)
C = np.array([ [5,-8],[2,-3] ])
C1 = inv(C)

# Associate letters of the "alphabet" with a matrix
matrices = {
  "A": A,
  "A1": inv(A),
  "B": B,
  "B1": inv(B),
  "C": C,
  "C1": inv(C)
}

def to_matrix(m):
  return matrices[m]

"""
  ms - list of matrices
  return - the product of matrices (product on the right)
    that is, the list ["A", "B"] returns the associated product AB, not BA
"""
def prod(ms):
  to_ret = ID
  for m in ms:
    to_ret = np.matmul(to_ret, m)
  return to_ret


P = A
N = prod([C,B])
As = A1
Q = B
#print(prod([P, N, As, inv(N), Q]))
#print(prod([C, A, C1, B, C, A, C1, B, C, A, C1]))

print(prod([C, A, C1, B, A, A, C, A1, C1]))
