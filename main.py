import os
import sys
import numpy as np
from numpy.linalg import inv

import pickle

# Setup matrices
ID = np.array([ [1,0],[0,1] ])
e1 = np.array([ [1], [0] ])
e2 = np.array([ [0], [1] ])

A = np.array([ [0,-1],[1,0] ])
B = np.array([ [1,4],[0,1] ])
C = np.array([ [5,-8],[2,-3] ])

# Associate letters of the "alphabet" with a matrix
matrices = {
  "A": A,
  "A1": inv(A),
  "B": B,
  "B1": inv(B),
  "C": C,
  "C1": inv(C)
}

"""
  m - a matrix
  x - a vector
  return - True if the vector mx is either e1 or e2
"""
def is_solution(m, x):
  vec = np.matmul(m, x).round()
  val = [ [ vec[0] ], [vec[1] ] ] # I have no idea why this needs to be done...
  return np.array_equal(e1,val) or np.array_equal(e2,val)

"""
  x - a 2d vector

  Helper function to get a reference for the explored/frontier sets for the vector x fromt he /data directory
"""
def filename_base(x):
  return "data/" + str(x[0]) + "_" + str(x[1]) + "__"

"""
  m - a key for the matrices dictionary
  Returns the associated matrix
"""
def to_matrix(m):
  return matrices[m]

"""
  ms - list of matrices
  return - the product of matrices (product on the right)
    that is, the list ["A", "B"] returns the associated product AB, not BA
"""
def prod(ms):
  to_ret = ID
  for m in ms:
    to_ret = np.matmul(to_ret, m)
  return to_ret

"""
  simple helper function to save data the same way
"""
def save(filename, data):
  with open(filename, "wb") as f:
    pickle.dump(data, f)
  

# Maximum length of the words to search
DEPTH = 3
if len(sys.argv) > 1:
  DEPTH= int( sys.argv[1] )

print("Depth:", DEPTH)

"""
  x - a vector to search for
  depth - the max length of the word to search

  This function performs a Breadth-First Search on the length of words to find asolution to: mx = e1 or mx = e2.
  It "caches" runs using the /data directory for each separate vector, x, and runs can be stopped/skipped at any point.
  If a solution was found on a previous run, that solution will be returned!

  For example, if a particular vector is taking too long to compute hitting CTRL + c will stop the search for that vector, but will save the current explored/frontier set to be picked up at a later time.

  Returns either None (if no solution is found) or a triple (x, word, m) with the vector, word solution, and associated matrix if a solution exists.

  NOTE: With large depths, this can take a while to run because it has to load the entire pickled explored/frontier files, which can get quite large.
"""
def search(x, depth=DEPTH):
  print(x, "SEARCH")
  explored = []
  frontier = [ ([],ID) ]
  solution = None
  did_search = False
  
  # Load previously searched words
  if os.path.exists(filename_base(x) + "explored.pkl"):
    print(x, "LOAD EXPLORED")
    with open(filename_base(x) + 'explored.pkl', 'rb') as f:
      explored = pickle.load(f)

  # Check if we have previously explored this vector
  # If we have and we found a solution, it will be the last element in the explored set.
  # So we can check if that element is a solution to this, if so no need to search. If it is not a solution, then continue on!
  if explored:
    word = explored[-1]
    m = prod( map( to_matrix, word ) )
    if is_solution(m,x):
      print(x, "PREVIOUS SOLUTION FOUND")
      frontier = [] # just in case this wasn't set before
      save(filename_base(x) + "frontier.pkl", frontier)
      solution = (x, word, m)
    else: # Clean up previous runs to clear out the explored set as it's unneeded.
      print(x, "NO PREVIOUS SOLUTION")
      explored = [ word ]
      save(filename_base(x) + "explored.pkl", explored)

  # Now load the frontier
  if os.path.exists(filename_base(x) + "frontier.pkl"):
    print(x, "LOAD FRONTIER")
    with open(filename_base(x) + 'frontier.pkl', 'rb') as f:
      frontier = pickle.load(f)
  
  """
    The main bulk of the code is wrapped in a try/finally so that we can interrupt the computation (via CTRL + c) but still cache our computation.
  """
  try:
    runs = 1
    print(x, "BEGIN SEARCH")
    while frontier:
      # Get the next word/matrix to explore
      (word, m) = frontier.pop(0)
    
      # Include some output so we know where the program is at.
      if runs % 1000 == 0:
        print(x, " run #" + str(runs) )

      # Check if we've reached our word length limit
      if len(word) > DEPTH:
        break

      # No need to check if word is in explored set.

      # A simple check to see if we are actually doing any work. If we've already calculated the words in a previous run (and aren't increasing the depth) we didn't search anything
      did_search = True
      runs += 1

      # Check if we've found the right matrix
      if is_solution(m, x):
        solution = (x, word, m)
        explored.append(word)
        print(x, "FOUND ONE!")
        break
    
      # Generate the next words from this current word
      # Don't allow inverses to be adjacent; i.e. only allow reduced words
      next_words = [ (word + [n], np.matmul(m, matrices[n] ).round() ) for n in matrices.keys() if len(word) == 0 or (len(word) > 0 and n != word[-1] + "1" and word[-1] != n + "1") ]
    
      # Add the new words to the list of words to be explored.
      frontier.extend(next_words)
  except KeyboardInterrupt:
    # Get some output in the terminal to let us know that we're interrupting the computation
    print("Saving before skipping...")
  finally:
    """
       CAVEAT!!! If we've found a solution, we don't care about the frontier OR the full explored set. We only want the solution.
    """
    print(x, "SEARCH ENDED")
    if solution:
      frontier = []
      explored = [ explored[-1] ]

    # Store what we've done to pick up on next time, but only if we actually searched
    if did_search:
      print(x, "SAVING")
      save(filename_base(x) + "explored.pkl", explored)
      save(filename_base(x) + "frontier.pkl", frontier)

    print(x, "RETURN")
    return solution

""" 
 I pulled this list of rationals from the JungleGym.pdf list. We can add more as we please/need!
"""
fracs = [(1, 4), (1, 8), (1, 12), (4, 15), (1, 16), (4, 17), (13, 18), (1, 20), (1, 24), (18, 25), (1, 28), (4, 31), (8, 31), (1, 32), (4, 33), (8, 33), (13, 34), (1, 36), (1, 40), (1, 44), (29, 46), (4, 47), (12, 47), (18, 47), (34, 47), (1, 48), (4, 49), (12, 49), (1, 52), (1, 56), (15, 56), (18, 59), (1, 60), (4, 63), (8, 63), (16, 63), (1, 64), (15, 64), (17, 64), (4, 65), (8, 65), (16, 65), (1, 68), (13, 70), (29, 70), (1, 72), (17, 72), (61, 72), (46, 73), (1, 76), (4, 79), (20, 79), (1, 80), (4, 81), (20, 81), (25, 82), (59, 82), (1, 84), (18, 85), (72, 85), (13, 86), (1, 88), (34, 89), (1, 92), (4, 95), (8, 95), (12, 95), (24, 95), (1, 96), (4, 97), (8, 97), (12, 97), (18, 97), (24, 97), (70, 97), (1, 100), (76, 103), (1, 104), (1, 108), (4, 111), (28, 111), (46, 111), (70, 111), (1, 112), (65, 112), (4, 113), (28, 113), (1, 116), (15, 116), (31, 116), (25, 118), (85, 118), (18, 119), (86, 119), (1, 120), (31, 120), (76, 121), (13, 122), (34, 123), (1, 124), (15, 124), (33, 124), (4, 127), (8, 127), (16, 127), (32, 127), (1, 128), (31, 128), (33, 128), (4, 129), (8, 129), (16, 129), (32, 129), (18, 131), (1, 132), (17, 132), (31, 132), (1, 136), (33, 136), (13, 138), (1, 140), (17, 140), (33, 140), (109, 142), (4, 143), (12, 143), (36, 143), (1, 144), (4, 145), (12, 145), (36, 145), (1, 148), (65, 148), (34, 149), (116, 151), (1, 152), (47, 154), (59, 154), (46, 155), (1, 156), (18, 157), (4, 159), (8, 159), (20, 159), (40, 159), (1, 160), (4, 161), (8, 161), (20, 161), (40, 161), (29, 162), (145, 162), (1, 164), (103, 164), (121, 164), (1, 168), (18, 169), (70, 169), (116, 169), (122, 169), (47, 170), (123, 170), (1, 172), (61, 172), (97, 172), (13, 174), (4, 175), (44, 175), (1, 176), (15, 176), (47, 176), (4, 177), (44, 177), (1, 180), (162, 181), (25, 182), (125, 182), (131, 182), (34, 183), (70, 183), (76, 183), (1, 184), (15, 184), (47, 184), (49, 184), (142, 185), (29, 186), (1, 188), (13, 190), (4, 191), (8, 191), (12, 191), (16, 191), (18, 191), (24, 191), (48, 191), (138, 191), (162, 191), (1, 192), (47, 192), (49, 192), (4, 193), (8, 193), (12, 193), (16, 193), (24, 193), (48, 193), (112, 193), (1, 196), (1, 200), (17, 200), (47, 200), (49, 200)]

"""
Holy damn. I never realized how much multiprocessing can help. Because our searches are all independent, we can run them all in parallel!
That literally what we do. Spin up a few workers and then give each of them a vector to search for.

This has *significantly* improved computation time!
"""
from multiprocessing import Pool

with Pool(4) as p:
  searches = p.map(search, fracs)
  
  p.close()

  print("Search Completed")

  # Filer out the solutions that are None (i.e. no solution was found yet)
  # Helper function to filter the search results
  def is_not_none(s):
    return s is not None
  
  solved = list( filter(is_not_none, searches) )

  print("Search Filtered")

  def to_frac_word(solution):
    (x, word, m) = solution
    return (x,word)

  cleaned = list( map(to_frac_word, solved) )
  
  print(cleaned)
  
  with open("solutions.txt", "w") as f:
    f.write(str(cleaned))
  
  print("Solutions Stored")
